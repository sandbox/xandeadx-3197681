<?php

namespace Drupal\xsubscription\Plugin\Block;

use Drupal\Core\Block\Attribute\Block;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\xsubscription\Form\SubscriptionForm;

#[Block(
  id: 'subscription_form_block',
  admin_label: new TranslatableMarkup('Subscription form'),
  category: new TranslatableMarkup('Forms'),
)]
class SubscriptionFormBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build(): array {
    return [
      'form' => \Drupal::formBuilder()->getForm(SubscriptionForm::class),
    ];
  }

}
