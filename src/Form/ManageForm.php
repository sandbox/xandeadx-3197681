<?php

namespace Drupal\xsubscription\Form;

use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\xsubscription\SubscriptionsStorage;

class ManageForm extends FormBase {

  /**
   * {@inheritDoc}
   */
  public function getFormId(): string {
    return 'subscriptions_manage_form';
  }

  /**
   * {@inheritDoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $subscriptions_storage = \Drupal::service('subscriptions_storage'); /** @var SubscriptionsStorage $subscriptions_storage */
    $date_formatter = \Drupal::service('date.formatter'); /** @var DateFormatterInterface $date_formatter */
    $emails = $subscriptions_storage->getAll();
    $rows = [];

    foreach ($emails as $email) {
      $rows[$email->id] = [
        'email'   => $email->email,
        'created' => $date_formatter->format($email->created, 'datetime'),
        'ip'      => $email->ip,
      ];
    }

    $form['emails'] = [
      '#type' => 'tableselect',
      '#header' => [
        'email'   => 'E-mail',
        'created' => $this->t('Date created'),
        'ip'      => 'IP',
      ],
      '#options' => $rows,
      '#empty' => $this->t('Empty'),
    ];

    $form['delete'] = [
      '#type' => 'submit',
      '#value' => $this->t('Delete'),
    ];

    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $subscriptions_storage = \Drupal::service('subscriptions_storage'); /** @var SubscriptionsStorage $subscriptions_storage */

    foreach ($form_state->getValue('emails') as $id => $state) {
      if ($state) {
        $subscriptions_storage->delete($id);
      }
    }

    $this->messenger()->addMessage($this->t('E-mails deleted'));
  }

}
