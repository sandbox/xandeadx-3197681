<?php

namespace Drupal\xsubscription\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure Simple Subscription settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'xsubscription_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return ['xsubscription.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $config = $this->config('xsubscription.settings');

    $form['text_before'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Text before form'),
      '#default_value' => $config->get('text_before'),
    ];

    $form['text_after'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Text after form'),
      '#default_value' => $config->get('text_after'),
    ];

    $form['success_message'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Success message'),
      '#default_value' => $config->get('success_message'),
    ];

    $form['button_text'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Button text'),
      '#default_value' => $config->get('button_text'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $this->config('xsubscription.settings')
      ->set('text_before', $form_state->getValue('text_before'))
      ->set('text_after', $form_state->getValue('text_after'))
      ->set('success_message', $form_state->getValue('success_message'))
      ->set('button_text', $form_state->getValue('button_text'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
