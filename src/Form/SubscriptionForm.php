<?php

namespace Drupal\xsubscription\Form;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\OpenModalDialogCommand;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Render\Markup;
use Drupal\xsubscription\SubscriptionsStorage;

class SubscriptionForm extends FormBase {

  /**
   * {@inheritDoc}
   */
  public function getFormId(): string {
    return 'subscription_form';
  }

  /**
   * {@inheritDoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $module_settings = $this->config('xsubscription.settings');

    if ($text_before = $module_settings->get('text_before')) {
      $form['text_before'] = [
        '#type' => 'item',
        '#markup' => Markup::create($text_before),
        '#weight' => -100,
      ];
    }

    $form['email'] = [
      '#type' => 'email',
      '#title' => 'E-mail',
      '#title_display' => 'invisible',
      '#attributes' => ['placeholder' => $this->t('E-mail')],
      '#required' => TRUE,
    ];

    $form['actions'] = [
      '#type' => 'actions',
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $module_settings->get('button_text') ?: $this->t('Subscribe'),
      '#ajax' => [
        'callback' => '::ajaxSubmit',
        'event' => 'click',
      ],
    ];

    if ($text_after = $module_settings->get('text_after')) {
      $form['text_after'] = [
        '#type' => 'item',
        '#markup' => Markup::create($text_after),
        '#weight' => 100,
      ];
    }

    // Enable block caching for authenticated users
    $form['#token'] = FALSE;

    $form['#attributes']['novalidate'] = 'novalidate';

    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state): void {
    $email = $form_state->getValue('email');
    $subscriptions_storage = \Drupal::service('subscriptions_storage'); /** @var SubscriptionsStorage $subscriptions_storage */
    if ($subscriptions_storage->emailExists($email)) {
      $form_state->setErrorByName('email', $this->t('You have already subscribed to this address.'));
    }

    if (!\Drupal::flood()->isAllowed('subscription', 1)) {
      $form_state->setErrorByName('', $this->t('Flood detected.'));
    }
  }

  /**
   * {@inheritDoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    // Add address to database
    $user_email_address = $form_state->getValue('email');
    $subscriptions_storage = \Drupal::service('subscriptions_storage'); /** @var SubscriptionsStorage $subscriptions_storage */
    $subscriptions_storage->add($user_email_address);

    \Drupal::flood()->register('subscription');

    $module_settings = $this->config('xsubscription.settings');
    $this->messenger()->addMessage($module_settings->get('success_message'));

    \Drupal::moduleHandler()->invokeAll('subscription_complete', [$user_email_address]);
  }

  /**
   * Ajax submit callback.
   */
  public function ajaxSubmit(array &$form, FormStateInterface $form_state): AjaxResponse {
    $response = new AjaxResponse();
    $module_settings = $this->config('xsubscription.settings');

    if ($form_state->hasAnyErrors()) {
      $response->addCommand(new OpenModalDialogCommand($this->t('Error'), [
        '#markup' => implode('<br /><br />', $form_state->getErrors()),
        '#attached' => [
          'library' => ['core/drupal.dialog.ajax'],
        ],
      ]));
      $this->messenger()->deleteByType(MessengerInterface::TYPE_ERROR);
    }
    else {
      $response->addCommand(new OpenModalDialogCommand('', [
        '#markup' => $module_settings->get('success_message'),
        '#attached' => [
          'library' => ['core/drupal.dialog.ajax'],
        ],
      ]));
      $this->messenger()->deleteByType(MessengerInterface::TYPE_STATUS);
    }

    return $response;
  }

}
