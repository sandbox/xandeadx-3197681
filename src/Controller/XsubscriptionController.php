<?php

namespace Drupal\xsubscription\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\xsubscription\SubscriptionsStorage;
use Symfony\Component\HttpFoundation\Response;

class XsubscriptionController extends ControllerBase {

  /**
   * Delete mail address from database.
   */
  public function deleteAddress($id): Response {
    $subscriptions_storage = \Drupal::service('subscriptions_storage'); /** @var SubscriptionsStorage $subscriptions_storage */
    $subscriptions_storage->delete($id);

    $this->messenger()->addMessage(t('Subscription deleted.'));

    return $this->redirect('xsubscription.address.manage');
  }

  /**
   * Export addresses.
   */
  public function export(): Response {
    $subscriptions_storage = \Drupal::service('subscriptions_storage'); /** @var SubscriptionsStorage $subscriptions_storage */
    $content = '';
    $addresses = $subscriptions_storage->getAll();

    foreach ($addresses as $address) {
      $content .= $address->email . "\n";
    }

    $response = new Response();
    $response->headers->set('Content-Type', 'text/csv; charset=utf-8');
    $response->headers->set('Content-Disposition', 'attachment; filename="addresses.csv"');
    $response->setContent($content);

    return $response;
  }

}
