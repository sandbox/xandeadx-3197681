<?php

namespace Drupal\xsubscription;

use Drupal\Core\Database\Connection;

class SubscriptionsStorage {

  public const TABLE = 'subscriptions';

  /**
   * The database connection.
   */
  protected Connection $database;

  /**
   * Class constructor.
   */
  public function __construct(Connection $database) {
    $this->database = $database;
  }

  /**
   * Add e-mail address.
   */
  public function add($email): bool {
    if (!$this->emailExists($email)) {
      $this->database
        ->insert(static::TABLE)
        ->fields([
          'email' => $email,
          'ip' => \Drupal::request()->getClientIp(),
          'created' => \Drupal::time()->getCurrentTime(),
        ])
        ->execute();

      return TRUE;
    }

    return FALSE;
  }

  /**
   * Return TRUE if e-mail exists in database.
   */
  public function emailExists($email): bool {
    return (bool)$this->database
      ->select(static::TABLE, 's')
      ->fields('s', ['id'])
      ->condition('s.email', $email)
      ->range(0, 1)
      ->execute()
      ->fetchField();
  }

  /**
   * Return all e-mails with info.
   */
  public function getAll(): array {
    return $this->database
      ->select(static::TABLE, 's')
      ->fields('s')
      ->orderBy('s.id', 'DESC')
      ->execute()
      ->fetchAll();
  }

  /**
   * Delete address by id.
   */
  public function delete($id): int {
    return $this->database
      ->delete(static::TABLE)
      ->condition('id', $id)
      ->execute();
  }

  /**
   * Table schema.
   */
  public static function schemaDefinition(): array {
    return [
      'fields' => [
        'id' => [
          'type' => 'serial',
          'unsigned' => TRUE,
          'not null' => TRUE,
        ],
        'email' => [
          'type' => 'varchar',
          'length' => 255,
          'not null' => TRUE,
          'default' => '',
        ],
        'ip' => [
          'type' => 'varchar_ascii',
          'length' => 40,
          'not null' => TRUE,
          'default' => '',
        ],
        'created' => [
          'type' => 'int',
          'unsigned' => TRUE,
          'not null' => TRUE,
          'default' => 0,
        ],
      ],
      'primary key' => ['id'],
    ];
  }

}
